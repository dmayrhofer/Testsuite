<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>MaxwellSlipBC2D_fine</title>
    <authors>
      <author>dmayrhof</author>
      <author>fspoerk</author>
    </authors>
    <date>2022-07-20</date>
    <keywords>
      <keyword>flow</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> 
      none
    </references>
    <isVerified>no</isVerified>
    <description>
      2D channel where a wave is propagating in positive y-direction. Artificial values for the viscosity and the scaling of the Maxwell BC lead to a certain slip at the outer channel wall. The slip velocity itself is determined with the solution dependent Maxwell slip BC where the slip velocity is dependent on the gradient of the normal velocity. This constraint gets enforced in weak sense with a Lagrange multiplier. Here we use a very finely resolved mesh to numerically verify the fulfillment of the already simplified PDE with respect to the boundary condition for this case.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <cdb fileName="Channel_fine.cdb"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <regionList>
      <region name="Chamber" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="Exc"/>
      <surfRegion name="Sym"/>
      <surfRegion name="BC_trans"/>
      <surfRegion name="BC_Fix"/>
      <surfRegion name="BC_top"/>
    </surfRegionList> 
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="orderVel">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <Lagrange id="orderLag">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="orderPres">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <integrationSchemeList>
    <scheme id="integVel">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="integLag">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="integPres">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  
  <sequenceStep index="1">
    <analysis>
      <transient>
        <numSteps>15</numSteps>
        <deltaT>2.5e-5</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <fluidMechLin formulation="compressible" presPolyId="orderPres" velPolyId="orderVel" presIntegId="integPres" velIntegId="integVel">
        <regionList>
          <region name="Chamber"/>
        </regionList>

        
        <bcsAndLoads> 
          <noSlip name="Sym">
            <comp dof="x"/>         
          </noSlip>
          <noSlip name="BC_top">
            <comp dof="x"/>
            <comp dof="y"/>    
          </noSlip>

          <velocity name="Exc">
            <comp dof="y" value="sin(2*pi*1000*t)"/>
          </velocity>

          <velocityConstraint name="BC_Fix" volumeRegion="Chamber">
            <MaxwellFirstOrderSlip meanFreePath="68e-9" C1="1000" formulation="hollowIncompressibleStress"/>
          </velocityConstraint>

          <scalarVelocityConstraint name="BC_trans" volumeRegion="Chamber">
            <noPenetration/>
          </scalarVelocityConstraint>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="lagrangeMultiplier">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <nodeResult type="lagrangeMultiplier1">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <elemResult type="fluidMechStress">
            <allRegions outputIds="h5"/>
          </elemResult>
          <elemResult type="fluidMechCompressibleStress">
            <allRegions outputIds="h5"/>
          </elemResult>
          <elemResult type="fluidMechPressureTensor">
            <allRegions outputIds="h5"/>
          </elemResult>
          <elemResult type="fluidMechViscousStress">
            <allRegions outputIds="h5"/>
          </elemResult>
          <elemResult type="fluidMechTotalStress">
            <allRegions outputIds="h5"/>
          </elemResult>
          <surfElemResult type="fluidMechSurfaceTraction">
            <surfRegionList>
              <surfRegion name="BC_Fix" />
            </surfRegionList>
          </surfElemResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard/>
        </solutionStrategy>
      </system>
    </linearSystems>
  </sequenceStep>
  
  <!-- The second sequence step contains the exact same setup except the formulation of the MaxwellSlipBC -->
  <sequenceStep index="2">
    <analysis>
      <transient initialTime="zero">
        <numSteps>15</numSteps>
        <deltaT>2.5e-5</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <fluidMechLin formulation="compressible" presPolyId="orderPres" velPolyId="orderVel" presIntegId="integPres" velIntegId="integVel">
        <regionList>
          <region name="Chamber"/>
        </regionList>

        
        <bcsAndLoads> 
          <noSlip name="Sym">
            <comp dof="x"/>         
          </noSlip>
          <noSlip name="BC_top">
            <comp dof="x"/>
            <comp dof="y"/>    
          </noSlip>

          <velocity name="Exc">
            <comp dof="y" value="sin(2*pi*1000*t)"/>
          </velocity>

          <velocityConstraint name="BC_Fix" volumeRegion="Chamber">
            <MaxwellFirstOrderSlip meanFreePath="68e-9" C1="1000" formulation="incompressibleStress"/>
          </velocityConstraint>

          <scalarVelocityConstraint name="BC_trans" volumeRegion="Chamber">
            <noPenetration/>
          </scalarVelocityConstraint>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="lagrangeMultiplier">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <nodeResult type="lagrangeMultiplier1">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <elemResult type="fluidMechViscousStress">
            <allRegions outputIds="h5"/>
          </elemResult>
          <elemResult type="fluidMechTotalStress">
            <allRegions outputIds="h5"/>
          </elemResult>
          <surfElemResult type="fluidMechSurfaceTraction">
            <surfRegionList>
              <surfRegion name="BC_Fix" />
            </surfRegionList>
          </surfElemResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>
    
    <linearSystems>
      <system>
        <solverList>
          <pardiso/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
  
  <!-- The third sequence step contains the exact same setup except the formulation of the MaxwellSlipBC -->
  <sequenceStep index="3">
    <analysis>
      <transient initialTime="zero">
        <numSteps>15</numSteps>
        <deltaT>2.5e-5</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <fluidMechLin formulation="compressible" presPolyId="orderPres" velPolyId="orderVel" presIntegId="integPres" velIntegId="integVel">
        <regionList>
          <region name="Chamber"/>
        </regionList>

        
        <bcsAndLoads> 
          <noSlip name="Sym">
            <comp dof="x"/>         
          </noSlip>
          <noSlip name="BC_top">
            <comp dof="x"/>
            <comp dof="y"/>    
          </noSlip>

          <velocity name="Exc">
            <comp dof="y" value="sin(2*pi*1000*t)"/>
          </velocity>

          <velocityConstraint name="BC_Fix" volumeRegion="Chamber">
            <MaxwellFirstOrderSlip meanFreePath="68e-9" C1="1000" formulation="compressibleStress"/>
          </velocityConstraint>

          <scalarVelocityConstraint name="BC_trans" volumeRegion="Chamber">
            <noPenetration/>
          </scalarVelocityConstraint>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="lagrangeMultiplier">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <nodeResult type="lagrangeMultiplier1">
            <allRegions outputIds="h5"/>
          </nodeResult>
          <elemResult type="fluidMechViscousStress">
            <allRegions outputIds="h5"/>
          </elemResult>
          <elemResult type="fluidMechTotalStress">
            <allRegions outputIds="h5"/>
          </elemResult>
          <surfElemResult type="fluidMechSurfaceTraction">
            <surfRegionList>
              <surfRegion name="BC_Fix" />
            </surfRegionList>
          </surfElemResult>
        </storeResults>
      </fluidMechLin>
    </pdeList>
    
    <linearSystems>
      <system>
        <solverList>
          <pardiso/>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
