<?xml version="1.0"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
  
  <documentation>
    <title>TransistorTempDep</title>
    <authors>
      <author>seiser</author>
    </authors>
    <date>2014-02-19</date>
    <keywords>
      <keyword>electrostatic</keyword>
    </keywords>
    <references>
      my bloody mind
    </references>
    <isVerified>no</isVerified>
    <description>
      A temperature dependant transistor in a short-circuit application. 
      The conductivity of the channel region depends on the temperature, the gate-source voltage and the drain-source voltage. 
      This is actually a coupled problem, where elecConduction and heatConduction PDEs are iteratively coupled. 
      The test case demonstrates:
        - transient coupling of non-linear electric with non-linear thermal PDE
	- electrical TriPole, where a region's conductivity depends on two voltage drops (the TriPole has three terminals, on which the elecPotential is evaluated).
	- temperature dependency of the TriPole, thus requiring the evaluation of a trilinear table model (see mat.xml) introduced in rev 12980.

      Model of a vertical DMOS transitor:
        Three volume regions are stacked on top of each other. Silicon (nonlinear thermal conductivity and capacity), channel, and a top metallization. Electrical power dissipation is 
	significant in the channel region, generating heat (aka joule heating), which in turn affects the nonlinear and temperature dependant electrical conductivity. 

	Thermal b.c.: The bottom of the silicon volume is held at constant temperature.
	Electrical b.c.: Gate-source and drain-source voltages are given by tables (see C[23]*.dat), which were measured on a device which was short using a 10uH coil and 100mOhm resistor.

	The device heats up rapidly, causing the over-temperature protection to kick-in (not included, but gate-source voltage drops), which turns the device off. Turing off the coil, creates a self-inductance as can be seen by the jump 
	in the drain-source voltage curve. This moment results in siginficant power dissipation and can result in a damaged device. Therefor, industry application requires the knowledge of
	the maximum temperature in the silicon.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <gmsh fileName="wire-trans.msh"/>
    </input>
    <output>
      <hdf5 id="h5"/>
      <gmsh binaryFormat="no" id="gm"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="3d" printGridInfo="yes">
    <variableList>
    </variableList>
    <regionList>
      <region name="vWire1" material="Si8res"/>
      <region name="vWire2" material="dummy"/>
      <region name="vGate" material="dummy"/>
      <region name="vChannel" material="transistorTdep" />
    </regionList>
    <surfRegionList>
      <surfRegion name="hot"/>
      <surfRegion name="cold"/>
      <surfRegion name="Udrain"/>
      <surfRegion name="Usource"/>
      <surfRegion name="Ugate"/>
    </surfRegionList>
    <elemList>
      <elems name="singleCh">
                <list>
                    <freeCoord comp="x" start="0.0e-3" stop="0.11e-3" inc="0.1e-3"/>
                    <freeCoord comp="y" start="0.0e-3" stop="0.11e-3" inc="0.1e-3"/>
                    <fixedCoord comp="z" value="0.21e-3"/>
                </list>
      </elems>
    </elemList>
    <nodeList>
      <nodes name="drainPt">
             <coord x="0" y="0" z="0.22e-3"/>
      </nodes>
      <nodes name="sourcePt">
             <coord x="0" y="0" z="0.2e-3"/>
      </nodes>
      <nodes name="gatePt">
             <coord x="2e-3" y="2e-3" z="0"/>
      </nodes>
      <nodes name="loadPt">
             <coord x="0" y="0" z="0.24e-3"/>
      </nodes>
    </nodeList>


  </domain>
  
    <sequenceStep index="1">
        <analysis>
            <static></static>
        </analysis>
        <pdeList>
            <heatConduction>
                <regionList>
                    <region name="vWire1"/>
                    <region name="vWire2"/>
                    <region name="vChannel"/>
                </regionList>
                <bcsAndLoads>
                    <temperature name="cold" value="298"/>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="heatTemperature">
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </heatConduction>
        </pdeList>
    </sequenceStep>
    <sequenceStep index="2">
      <analysis>
          <!--<static></static>-->
         <transient>
            <numSteps>30</numSteps>
            <deltaT>1e-5</deltaT>
         </transient>
      </analysis>
      <pdeList>

            <heatConduction  systemId="heat">
                <regionList>
                    <region name="vWire1" nonLinIds="cond cap"/>
                    <region name="vWire2"/>
                    <region name="vChannel"/>
                </regionList>

                <nonLinList>
                   <heatConductivity id="cond"/>
                   <heatCapacity id="cap"/>
               </nonLinList>   

               <initialValues>
                  <initialState>
                    <sequenceStep index="1"/>
                 </initialState>
               </initialValues>

                <bcsAndLoads>
                     <temperature name="cold" value="298"/>
          	    <!-- <temperature name="vChannel" value="300+.99*sin(2000*2*pi*t)"/> --> <!-- Kelvin -->
          	    <!-- <temperature name="vChannel" value="298"/> --> <!-- Kelvin -->
		    <!-- <temperature name="vChannel" value="1*sample1D('temp_input.dat',t,1)"/> -->

                    <elecPowerDensity name="vChannel">
                      <coupling pdeName="elecConduction">
                          <quantity name="elecPowerDensity"/>
                      </coupling>        
                   </elecPowerDensity>          
                </bcsAndLoads>

                <storeResults>
                    <nodeResult type="heatTemperature">
                        <allRegions/>
			<nodeList>
			  <nodes name="drainPt" outputIds="txt"/> <!-- history/DiodeTempDep-ms2-heatTemperature-node-8-anoPt.hist -->
			  <nodes name="sourcePt" outputIds="txt"/> <!-- history/DiodeTempDep-ms2-heatTemperature-node-8-anoPt.hist -->
			</nodeList>
                    </nodeResult> 
                </storeResults>
            </heatConduction>
        <elecConduction systemId="elec">
          <regionList>
            <region name="vWire1" />
            <region name="vChannel" matDependIds="cond" nonLinIds="tripolemodel"/>
            <region name="vWire2"/>
            <region name="vGate"/>
          </regionList>

          <nonLinList>
            <elecTriPoleTempDep id="tripolemodel"/>
          </nonLinList>

        <matDependencyList>
            <elecConductivity id="cond">
              <coupling pdeName="heatConduction">
                 <quantity name="heatTemperature"/>
              </coupling>
            </elecConductivity>
        </matDependencyList>    

          <poleList>
              <Tripole id="tripolemodel">
                <gate region="Ugate"/>
                <drain region="Udrain"/>
                <source region="Usource"/>
              </Tripole>
          </poleList>

          <bcsAndLoads>
            <ground name="cold"/>
            <!-- <potential name="hot" value="10*sample1D('appx_vds_shifted.dat',t,1)"/> --> <!-- drain supply voltage, Volts -->
            <potential name="hot" value="10*sample1D('C3darts11_u1800002_down_shift6e-5.txt',t,1)"/> <!-- drain supply voltage, Volts -->
            <!-- <potential name="hot" value="50"/> --> <!-- drain supply voltage, Volts -->
            <!-- <potential name="Ugate" value="sample1D('appx_vgs_shifted.dat',t,1)"/> --> <!-- gate voltage modulation, volts -->
            <potential name="Ugate" value="sample1D('C2darts11_u1800002_down_shift6e-5.txt',t,1)"/> <!-- gate voltage modulation, volts -->
	    <!-- <potential name="Ugate" value="3.775" /> -->
          </bcsAndLoads>
          
          <storeResults>
            <nodeResult type="elecPotential">
              <allRegions/>
              <nodeList>
                <nodes name="sourcePt" outputIds="txt"/> <!-- history/ -->
                <nodes name="drainPt" outputIds="txt"/> <!-- history/ -->
                <nodes name="gatePt" outputIds="txt"/> <!-- history/ -->
                <nodes name="loadPt" outputIds="txt"/> <!-- history/  -->
              </nodeList>
            </nodeResult>
            <elemResult type="elecCurrentDensity">
              <allRegions/>
            </elemResult>
            <elemResult type="elecPowerDensity">
              <!--<allRegions/>-->
	      <regionList>
	        <region name="vChannel"/>
	        <region name="vWire1"/>
	        <region name="vWire2"/>
	      </regionList>
	      <elemList>
	        <elems name="singleCh"/>
	      </elemList>
            </elemResult>
            <regionResult type="elecPower">
              <!--<allRegions/>-->
	      <regionList>
	        <region name="vChannel"/>
	        <region name="vWire1"/>
	        <region name="vWire2"/>
	      </regionList>
            </regionResult>
           <surfElemResult type="elecNormalCurrentDensity">
              <surfRegionList>
                <surfRegion name="Udrain"/>
              </surfRegionList>
            </surfElemResult> 
           <surfRegionResult type="elecCurrent">
              <surfRegionList>
                <surfRegion name="Udrain"/> <!-- history/ -->

              </surfRegionList>
            </surfRegionResult>
          </storeResults>
        </elecConduction>
        </pdeList>
    <couplingList>
        <iterative>
            <convergence logging="yes" maxNumIters="5" stopOnDivergence="yes">
                <!-- <quantity name="elecCurrent" value="1e-3" normType="rel"/> -->
                <quantity name="elecPower" value="1e-3" normType="rel"/> 
            </convergence>
        </iterative>
    </couplingList>

    <linearSystems>
      <system id="heat">
        <solutionStrategy>
          <standard>
           <nonLinear logging="yes">
                   <lineSearch/>
                   <incStopCrit> 1e-3</incStopCrit>
                   <resStopCrit> 1e-3</resStopCrit>
                   <maxNumIters> 20  </maxNumIters>
            </nonLinear> 
          </standard>
        </solutionStrategy>
        <!--<solverList>
          <pardiso>
          </pardiso>
        </solverList>-->
      </system>
      <system id="elec">
        <solutionStrategy>
          <standard>
                        <nonLinear logging="yes">
                            <lineSearch type="none"/>
                            <incStopCrit> 1e-7</incStopCrit>
                            <resStopCrit> 1e-7</resStopCrit>
                            <maxNumIters> 20  </maxNumIters>
                        </nonLinear>
          </standard>
        </solutionStrategy>
        <!--<solverList>
          <pardiso/>
        </solverList> -->
      </system>
    </linearSystems>
    </sequenceStep>
  
</cfsSimulation>
