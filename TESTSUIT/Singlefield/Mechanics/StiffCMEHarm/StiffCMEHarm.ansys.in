! ----------------------- ! ! Description: ! ----------------------- !
!   This is an ANSYS classic input script that will create the mesh
!   for this test case.
! -------------------------------------------------------------------------- !


! ----------------------- !
! Initialize ANSYS:
! ----------------------- !
FINI
/CLEAR
/filname,StiffCMEHarm
/PREP7
init

! ----------------------- !
! User Defined Parameters:
! ----------------------- !
! spring stiffness of concentrated element (in N/m)
*SET,K,20e9

! desired weight of discretized structure
*SET,M,1

! density of the mass material (in N/mm^2)
*SET,RHO,7000

! distance from spring to mass
*SET,dist,1e-2

! element size
*SET,esz,1e-2

! =============================================================== !
! Preprocessing
! =============================================================== !

! ----------------------- !
! Create Geometry:
! ----------------------- !
! calculate dimension of the mass part to match the total desired
! mass.
*SET,length,sqrt(M/RHO)

! create left area for fixing the structure
RECTNG,0,dist/5,-length/2,0
RECTNG,0,dist/5,0,length/2
NUMMRG,KP
CM,fix_A,AREA

RECTNG,dist,length+dist,-length/2,0
RECTNG,dist,length+dist,0,length/2
ALLSEL
NUMMRG,KP
CMSEL,U,fix_A
CM,mass_A,AREA

! middle keypoints to attach the spring to
KSEL,S,LOC,X,dist/5,dist
KSEL,R,LOC,Y,0
CM,stiffPoint_KP,KP

! history / excitation keypoint
KSEL,S,LOC,X,dist
KSEL,R,LOC,Y,0
CM,hist_KP,KP

! ----------------------- !
! Create Mesh:
! ----------------------- !
setelems,'quadr'
esize,esz
ASEL,ALL
AMESH,ALL

! =============================================================== !
! Write the mesh
! =============================================================== !

allsel
wnodes

cmsel,s,mass_A
esla
welems,'mass'

cmsel,s,fix_A
esla
welems,'fix'

cmsel,s,stiffPoint_KP
nslk
wnodbc,'stiffPoint'

cmsel,s,hist_KP
nslk
wnodbc,'hist'


allsel
mkmesh
