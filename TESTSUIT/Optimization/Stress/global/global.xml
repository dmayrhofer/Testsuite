<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation">
  <documentation>
    <title>global stress constraints (penalized)</title>
    <authors>
      <author>Fabian Wein</author>
    </authors>
    <date>2020-12-30</date>
    <keywords>
      <keyword>SIMP</keyword>
    </keywords>
    <references>standard</references>
    <isVerified>no</isVerified>
    <description>
       globalized stress constraints 1/N sum max((sigma, M sigma) - parameter, 0)^power smaller equal value 
       where 1/N is enabled normalization. Very poor convergence, takes 1000 iterations for a 40-mesh.
       See local_vts and local_penal descriptions. Note that for solid quadraticVMStress = vonMisesStress**2
       with a small discrepance due to finer element integration for the former.
    </description>
  </documentation>
  
  <fileFormats>
    <output>
      <hdf5/>
      <info/>
    </output>
    <materialData file="mat.xml" format="xml" />
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region name="mech" material="99lines" /> 
      <region name="inner" material="99lines" />
    </regionList>
    <elemList>
      <elems name="center_el">
        <!-- we assume even number of cells, so there is no center element. Take the right upper to be more deterministic -->
        <coord x=".5001" y=".5001" />
       </elems>
    </elemList>
    <nodeList>
      <nodes name="center">
        <coord x=".5" y=".5"/>
      </nodes>
      <nodes name="load">
        <list>
          <freeCoord comp="y" start=".4" stop=".6" inc=".01" />
          <fixedCoord comp="x" value="1"/>
        </list>
      </nodes>
    </nodeList>
    
  </domain>

  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="planeStress">
        <regionList>
          <region name="mech" />
          <region name="inner" />
        </regionList>

        <bcsAndLoads>
           <fix name="west"> 
              <comp dof="x"/> 
              <comp dof="y"/> 
           </fix>
           <force name="load">
             <comp dof="x" value="1"/>
           </force>
        </bcsAndLoads>

        <storeResults>
          <nodeResult type="mechDisplacement">
            <allRegions/>
            <nodeList>
              <nodes name="center"/>
            </nodeList>
          </nodeResult>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="physicalPseudoDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="mechStrain">
            <allRegions/>
            <elemList>
              <elems name="center_el"/>
            </elemList>
          </elemResult>
          <elemResult type="mechStress">
            <allRegions/>
            <elemList>
              <elems name="center_el"/>
            </elemList>
          </elemResult>
          <elemResult type="vonMisesStrain" >
            <allRegions/>
            <elemList>
              <elems name="center_el"/>
            </elemList>
          </elemResult>
          <elemResult type="vonMisesStress" >
            <allRegions/>
            <elemList>
              <elems name="center_el"/>
            </elemList>
          </elemResult>

          <elemResult type="optResult_1">
            <allRegions/>
            <elemList>
              <elems name="center_el"/>
            </elemList>
          </elemResult>

          <elemResult type="optResult_2">
            <allRegions/>
          </elemResult>
          
          <regionResult type="mechDeformEnergy">
            <allRegions/>
          </regionResult>
          
        </storeResults>
      </mechanic>
    </pdeList>

   <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <matrix reordering="noReordering"/>
          </standard>
        </solutionStrategy>        
<!--         <solverList> -->
<!--           <cholmod/> -->
<!--         </solverList>  -->
      </system>
    </linearSystems> 
  </sequenceStep>

    
  <optimization>
    <costFunction type="compliance" task="minimize" >
      <stopping queue="999" value="0.001" type="designChange"/>
    </costFunction>

    <constraint type="volume" value=".5" bound="upperBound" linear="true" mode="constraint"  />


    <!--  1/N sum max((sigma, M sigma) - parameter, 0)^power <= value  -->
    <constraint type="globalStress" value=".0001" bound="upperBound" region="inner" parameter="2" >
      <local power="2" normalize="true"/>
    </constraint>

    <!-- allow for 1000 iterations with bulk2d_40_ball_0_3.mesh for almost KKT  -->
    <optimizer type="snopt" maxIterations="1000">
      <snopt>
        <option name="verify_level" type="integer" value="-1"/>
      </snopt>
    </optimizer>

    <ersatzMaterial material="mechanic" method="simp" >
      <regions>
        <region name="mech"/>
        <region name="inner"/>
      </regions>
      <filters>
        <filter neighborhood="maxEdge" value="1.5" type="density"/>
      </filters>

      <design name="density" initial=".99" physical_lower="1e-5" upper="1.0" />

      <transferFunction type="simp" application="mech" param="3"/>
      <transferFunction type="simp" application="stress" design="density" param=".3"/>

      <result value="quadraticVMStress" id="optResult_1"/>
      <result value="constraintGradient" detail="greyness" id="optResult_2"/>
      <export save="last" write="iteration" compress="false"/>
    </ersatzMaterial>
    <commit mode="each_forward" stride="99"/>
   </optimization>
</cfsSimulation>


